The file data.csv has the input data. 

The file strategy.cpp has the entire code for the assignment. 

The file series-data.csv has all the all intermediate values used for the portfolio creation, like the beta series , spread series 
present portfolio values 

The file df-res.csv has all the daily returns values on the portfolio created

Specifically , the main functions in the code are :

betaCal(dataframe) - calculating the beta series for a dataframe

spreadCal(dataframe,beta) - calculating the spread series for a dataframe

technicalIndicator(dataframe) - creating the technical indicators for the strategy 

holdings(dataframe,beta) - calculating the daily portfolio returns