#include <bits/stdc++.h>
using namespace std;

// implementation of a dataframe
class dataframe{

	public : 
		dataframe();
		dataframe(string);
		dataframe(vector< vector<string> >); 
		vector< vector<string> > df;
		vector<string> columns; 
		std::map<std::string, int> columnMapping;
		std::map<std::string, int> indexMap; 
		void addDF(dataframe d2);

		vector<string> getColumnNames();
		dataframe searchByValue(string columnName,string val);
		void writeCsv(string);
		void setIndex();
		void insertColumn(string , vector<string>);
		int len(); 
};

// setting up a dataframe using a csv file
dataframe::dataframe(string fileName){

	ifstream data(fileName.c_str());
    string line;
    vector< vector<string> > df; 
    vector<string> row;
    while(getline(data,line))
    {
    	line = line.substr(0, line.size()-1);
        stringstream lineStream(line);
        string cell;
        while(getline(lineStream,cell,','))
        {
        	row.push_back(cell);
        }

        df.push_back(row);
        row.clear();
    }

    this->df = df ; 
    this->columns = df[0];

	for(int i= 0 ; i < df[0].size(); i++){
		this->columnMapping[df[0][i]] = i ; 
	}

	setIndex();
}

// setting up a dataframe using a 2d vector 
dataframe::dataframe(vector< vector<string> > df){

    this->df = df ; 
    this->columns = df[0];

	for(int i= 0 ; i < df[0].size(); i++){
		this->columnMapping[df[0][i]] = i ; 
	}    

	setIndex();
}

// len of a dataframe
int dataframe::len(){
	return this->df.size();
}

// empty constructor
dataframe::dataframe(){};

// returns the column names in the dataframe
vector<string> dataframe::getColumnNames(){
	return this->columns;
}

// searching the dataframe for a particular value
dataframe dataframe::searchByValue(string columnName,string val){
	
	int index = this->columnMapping[columnName];
	vector< vector<string> > arr ; 
	arr.push_back(this->columns);

	for (int i =0 ; i < this->df.size() ; i++){
		if(this->df[i][index] == val){
			arr.push_back(this->df[i]);
		}
	}
	dataframe d(arr);
	return d;
}

// writing a dataframe to a csv file
void dataframe::writeCsv(string fileName){

	ofstream myfile;
	myfile.open (fileName.c_str());
	string str;

	for(int i= 0;i < this->df.size() ; i++){
		str = "";
		for(int j = 0 ; j < this->df[0].size() ; j++){
			str += this->df[i][j];
			if(j!=this->df[0].size()-1) {
				str += ",";
			}
			else str += "\n";  
		}
		myfile << str;
	}
	myfile.close();	
}

// making combination of Date and time as index for making the query easy
void dataframe::setIndex(){

	int i_date = this->columnMapping["Date"];
	int i_time = this->columnMapping["Time"] ; 

	for (int i =0 ; i < this->df.size();i++){
		string s = this->df[i][i_date] + " " + this->df[i][i_time];
		this->indexMap[s] = i;
	}	
}

// printing the values in a dataframe
void printValues(dataframe d,int rows){

	for(int i= 0 ; i < rows; i++){
		for (int j= 0 ; j < d.df[0].size() ; j++){
			cout<<d.df[i][j]<<" ";
		}
		cout<<endl; 
	}
}

// adding a new dataframe 
void dataframe::addDF(dataframe d2){
	for (int i = 1 ; i < d2.len() ; i++){
		this->df.push_back(d2.df[i]);
	}  
}

// inserting a new column in a dataframe
void dataframe::insertColumn(string columnName,vector<string> values){

	this->df[0].push_back(columnName);
	int n = df[0].size()-1;

	for(int i = 0 ; i < values.size(); i++){
		this->df[i+1].push_back(values[i]);
	}

	this->columnMapping[columnName] = df[0].size()-1;
}


// creating an intersection of 2 dataframes
dataframe mergeDataFrames(dataframe d1,dataframe d2){

	vector< vector<string> > d ; 
	int i_date = d2.columnMapping["Date"];
	int i_time = d2.columnMapping["Time"] ; 
	string arr[] = {"Date","Time","Price_Nif","Price_Bank"};
	vector<string> columnNames (arr,arr+sizeof(arr) / sizeof(arr[0])); 
	d.push_back(columnNames);

	vector<string> row ; 
	for (int i =1 ; i < d2.len() ; i ++ ){
		string s = d2.df[i][i_date] + " " + d2.df[i][i_time];
		if(d1.indexMap.count(s) > 0){
			int i_d1 = d1.indexMap[s];
			int i_price1 = d1.columnMapping["Price"];
			int i_price2 = d2.columnMapping["Price"];

			row.push_back(d2.df[i][i_date]);
			row.push_back(d2.df[i][i_time]);
			row.push_back(d1.df[i_d1][i_price1]);	
			row.push_back(d2.df[i][i_price2]);	

			d.push_back(row);
	        row.clear();
		}
	}
	dataframe mrg(d);
	return mrg; 
}
 
namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

// calculating the beta series values for a dataframe and inserting into the dataframe. 
dataframe betaCal(dataframe d1){

	string columnName = "beta";
	vector<string> arr ; 

	vector< vector<string> > arr1 ;
	int i_price_nif = d1.columnMapping["Price_Nif"];
	int i_price_bank = d1.columnMapping["Price_Bank"];

	for(int i = 1 ; i < d1.len(); i++){
		float beta = atof(d1.df[i][i_price_bank].c_str())/atof(d1.df[i][i_price_nif].c_str());
		arr.push_back(patch::to_string(beta));	
	}

	d1.insertColumn(columnName,arr);
	return d1;

}

// creating the spread series for a dataframe and inserting the column in the dataframe
dataframe spreadCal(dataframe d1,float beta){

	string columnName = "spread";
	vector<string> arr ; 

	vector< vector<string> > arr1 ;
	int i_price_nif = d1.columnMapping["Price_Nif"];
	int i_price_bank = d1.columnMapping["Price_Bank"];

	float spread;
	for(int i = 1 ; i < d1.len(); i++){
		spread = atof(d1.df[i][i_price_bank].c_str()) - beta*atof(d1.df[i][i_price_nif].c_str());
		arr.push_back(patch::to_string(spread));	
	}

	d1.insertColumn(columnName,arr);
	return d1;
}


// finding the average a column in a dataframe 
float findAverage(dataframe d,string columnName){
	float s = 0.0 ; 
	int i_colIndex  = d.columnMapping[columnName];
	for (int i = 1 ; i < d.len();i++){
		s += atof(d.df[i][i_colIndex].c_str());
	}
	return s/(d.len()-1); 
}

// splitting a string using a delimiter
vector <string>  stringSplit(string s,char delimiter){
	stringstream data(s);
	vector<string> result; 
    string line;
    while(std::getline(data,line,delimiter))
    {
        result.push_back(line);
    }	
    return result;
}

// checking if the given time is less than equal to '12:00:00'
bool compareTimeWindow(string s_time){
	vector <string> str = stringSplit(s_time,':');
	return atoi(str[0].c_str()) < 12 ? 1 : (atoi(str[0].c_str()) == 12 && atoi(str[1].c_str()) == 0) ? 1 : 0 ; 
}

// calculating the rolling window size for every day
int rollingWindowSize(dataframe d1){
	int i_time = d1.columnMapping["Time"];
	int i ;
	for (i = 1; i < d1.len() ; i++){
		if(!compareTimeWindow(d1.df[i][i_time]))
			break;
	}
	return i-1;
}

// calculating the rolling mean for a column in a dataframe for a given window size
vector<string> RollingMean(dataframe d,string columnName,int rollingWindowSize){

	int i_column = d.columnMapping[columnName];
	vector<string> values ;
	float sm = 0;  
	for(int i =1 ; i < d.len(); i++){
		if(i < rollingWindowSize){
			values.push_back("NA");
			sm += atof(d.df[i][i_column].c_str()) ;
		}
		else{
			if (i>rollingWindowSize){
				sm -= atof(d.df[i-rollingWindowSize][i_column].c_str());
			}
			sm += atof(d.df[i][i_column].c_str());
			values.push_back(patch::to_string(sm/rollingWindowSize)) ; 
		}
	}

	return values;
}


//calculating the rolling standard deviation for column in a dataframe for a given window size
vector<string> RollingSD(dataframe d,string columnName,int rollingWindowSize){

	int i_column = d.columnMapping[columnName];
	vector<string> values ;

	float sm = 0;
	float sm_sq = 0;  
	float n =0 ; 
	float n_last = 0; 
	float mean = 0; 
	float variance = 0 ;
	float std = 0 ;

	for(int i =1 ; i < d.len(); i++){
		n = atof(d.df[i][i_column].c_str()); 
		if(i < rollingWindowSize){
			values.push_back("NA");
			sm += n ;
			sm_sq += n*n; 
		}
		else{
			n_last = atof(d.df[i-rollingWindowSize][i_column].c_str());
			if (i>rollingWindowSize){
				sm -= n_last;
				sm_sq -= n_last*n_last;
			}
			sm += n;
			sm_sq += n*n; 
			mean = sm/rollingWindowSize;
			variance = (sm_sq - rollingWindowSize*mean*mean) / (rollingWindowSize-1); 
			std = sqrt(variance);

			values.push_back(patch::to_string(std)) ; 
		}
	}
	return values;
}

/* creating the technical indicator for the given data 

The indicator values are as follows : 
" 1" - buy beta units of Nif and sell 1 unit of Bank 
"-1" - sell beta units of Nif and buy 1 unit of Bank
" 0" - no action / hold the earlier position
"-2" - end of the day => close all the remaining positions 

*/
dataframe technicalIndicator(dataframe d){

	int i_spread = d.columnMapping["spread"];
	int i_rolling_mean = d.columnMapping["rolling_mean"];
	int i_rolling_sd = d.columnMapping["rolling_sd"];
	float ti = 0 ;

	vector<string> indicator ; 
	string columnName = "indicator" ; 
	// indicator.push_back(columnName);
	float sprd = 0 ; 

	for(int i =1 ; i < d.len() ; i++){	
		if(i == d.len()-1)	{
			 indicator.push_back("-2");
		}
		else if(d.df[i][i_rolling_mean] != "NA"){
			float band_lower = atof(d.df[i][i_rolling_mean].c_str()) - atof(d.df[i][i_rolling_sd].c_str()) ; 
			float band_upper = atof(d.df[i][i_rolling_mean].c_str()) + atof(d.df[i][i_rolling_sd].c_str()) ; 
			sprd = atof(d.df[i][i_spread].c_str()) ; 

			if(sprd > band_upper) indicator.push_back("1");
			else if(sprd < band_lower) indicator.push_back("-1");
			else indicator.push_back("0");
		}
		else {
			indicator.push_back("NA");
		}		
	}

	d.insertColumn(columnName,indicator);
	return d;
}


/*
Based on the technical indicators defined above , calculate the holdings at any moment of the day 
We create a hypothetical portfolio with zero initial cash 
the portfolio value at any point is = current holfings * current price + in hand cash
the portfolio value at the end of the day gives me the total return for the day with my strategy
*/

dataframe holdings(dataframe d,float beta){

	int i_indicator = d.columnMapping["indicator"]; 
	int i_price_nif = d.columnMapping["Price_Nif"];
	int i_price_bank = d.columnMapping["Price_Bank"];

	vector<string> nif_holding ; 
	vector<string> bank_holding ;
	vector<string> in_hand_cash ; 
	vector<string> pv; 

	int n_nif = 0 , n_bank = 0 ;
	float cash = 0 ; 
	float portfolio_value = 0 ; 

	for (int i = 1 ; i < d.len(); i++){
		float cprice_nif = atof(d.df[i][i_price_nif].c_str());
		float cprice_bank = atof(d.df[i][i_price_bank].c_str()); 
		if(d.df[i][i_indicator] == "1") {
			n_nif += (int)beta;
			n_bank -= 1; 
			cash = cash - (int)beta*cprice_nif + cprice_bank; 
		}

		else if(d.df[i][i_indicator] == "-1"){
			n_nif -= (int)beta;
			n_bank += 1; 
			cash = cash + (int)beta*cprice_nif - cprice_bank; 
		}

		else if(d.df[i][i_indicator] == "-2"){
			cash = cash + n_nif*cprice_nif + n_bank*cprice_bank; 
			n_nif = 0 ; 
			n_bank = 0 ;
		}

		portfolio_value = cash + n_nif*cprice_nif + n_bank*cprice_bank;
		nif_holding.push_back(patch::to_string(n_nif));
		bank_holding.push_back(patch::to_string(n_bank));
		in_hand_cash.push_back(patch::to_string(cash));
		pv.push_back(patch::to_string(portfolio_value));
	}

	d.insertColumn("nif_holding",nif_holding);
	d.insertColumn("bank_holding",bank_holding);
	d.insertColumn("in_hand_cash",in_hand_cash);
	d.insertColumn("portfolio_value",pv);
	return d;
}

// creating a mapping between the dates in my dataframe and their beta values
std::map<std::string,float> betaMapping(dataframe d){

	int i_Date = d.columnMapping["Date"];
	int i_Beta = d.columnMapping["beta"]; 

	string t = d.df[1][i_Date];
	std::map<std::string,float>	betaMap ;  
	float beta = 0 ; 
	int cnt = 0 ; 

	for (int i = 1 ; i < d.len() ; i++){
		float b = atof(d.df[i][i_Beta].c_str());
		if(d.df[i][i_Date] == t){
			beta += b;
			cnt ++;
		}

		else{
			betaMap[t] = beta/cnt ; 
			t = d.df[i][i_Date];
			cnt = 1 ;
			beta = b; 		
		}
	}

	return betaMap; 
}


void printMap(std::map<std::string,float> mp){
	for (std::map<std::string,float>::iterator it=mp.begin(); it!=mp.end(); ++it){
		std::cout << it->first << " => " << it->second << '\n';
	}
}

vector<string> uniqueDates(dataframe d){

	int cnt = 0 ; 
	int  i_Date = d.columnMapping["Date"];
	string t = d.df[1][i_Date];

	vector<string> arr;
	arr.push_back(t);

	for(int i = 1; i < d.len() ; i++){
		if(d.df[i][i_Date] != t){
			arr.push_back(d.df[i][i_Date]);
			t = d.df[i][i_Date];
		}
	}
	return arr; 	
}

int main(){

	dataframe d("data.csv");
	dataframe d1 = d.searchByValue("Name","Nif");
	dataframe d2 = d.searchByValue("Name","Bank");

	dataframe dMerged = mergeDataFrames(d1,d2);

	dataframe dM = betaCal(dMerged);

	std::map<std::string,float> betaMap = betaMapping(dM);
	vector<string> dates = uniqueDates(dM) ;

	vector< vector <string> > results; 
	vector<string> results_row; 
	results_row.push_back("Date");
	results_row.push_back("beta"); 
	results_row.push_back("net_return"); 
	results.push_back(results_row);
	results_row.clear();
	bool f=  0 ; 
	dataframe df_final_metrics; 

	for(int i = 1 ; i < dates.size() ; i++){
		float beta = betaMap[dates[i-1]];
		dataframe df1 =  dMerged.searchByValue("Date",dates[i]);
		df1 = spreadCal(df1,beta);

		int windowSize = rollingWindowSize(df1);
		vector<string> arr = RollingMean(df1,"spread",windowSize);
		df1.insertColumn("rolling_mean",arr);
		arr = RollingSD(df1,"spread",windowSize);
		df1.insertColumn("rolling_sd",arr);

		df1 = technicalIndicator(df1);
		df1 = holdings(df1,beta);

		int i_portfolioValue = df1.columnMapping["portfolio_value"];

		results_row.push_back(dates[i]);
		results_row.push_back(patch::to_string(beta));
		int i_lastTime = df1.len()-1;
		results_row.push_back(df1.df[i_lastTime][i_portfolioValue]);

		results.push_back(results_row);
		results_row.clear();

		if(!f){
			df_final_metrics = df1;
			f=1;
		}
		else df_final_metrics.addDF(df1);
	} 

	dataframe df_res(results);
	df_res.writeCsv("df_res.csv");
	df_final_metrics.writeCsv("series_data.csv");
}